Pod::Spec.new do |spec|

  spec.name         = "MobileFramework"
  spec.version      = "2.0.3"
  spec.summary      = "A short description of MobileSDK.This framework is create for the test purpose of the project "
  spec.description  = "This is demo creation of mobileSDK just for testing.We are just try to create own framework so that we can use it anywhere in other application "

  spec.homepage     = "https://r_mane@bitbucket.org/r_mane/demomobilesdk.git"
  spec.license      = "DEMO"
  spec.author             = { "Rohit Mane" => "rmane@zarca.com" }

  spec.platform     = :ios, "9.0"

  spec.source       = { :git => "https://r_mane@bitbucket.org/r_mane/demomobilesdk.git", :tag => "#{spec.version}" }

  spec.source_files        = "MobileFramework"
  spec.resources = "MobileFramework/*.{png,jpeg,jpg,storyboard,xib,xcassets}"
  spec.swift_version       = "4.2"	
  	


 

  # spec.public_header_files = "Classes/**/*.h"
  

end

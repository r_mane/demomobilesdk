//
//  WekkitViewController.swift
//  MobileFramework
//
//  Created by Rohit Mane on 7/24/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import UIKit
import WebKit

public class WekkitViewController: UIViewController,WKNavigationDelegate {

    var webView: WKWebView!
    var urlString : URL?
    var myTitle = "Participate"
    var surveyURL:String?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.title = myTitle
        if((urlString) != nil){
            unarchive(archiveURL: urlString!)
        }else{
            showAlert(msg: "Some Error Occured")
        }
        
    }
    
    override public func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self as! WKNavigationDelegate
        view = webView
    }
    
    func unarchive(archiveURL : URL) {
        if FileManager.default.fileExists(atPath: archiveURL.path) {
            webView.loadFileURL(urlString!, allowingReadAccessTo: archiveURL)
        }
    }
    
    func showAlert(msg:String) {
        let alert = UIAlertController(title: "Alert", message: msg,preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

//
//  SurveyListViewController.swift
//  MobileFramework
//
//  Created by Rohit Mane on 11/10/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import UIKit

class SurveyListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var surveyList = [Survey]()
    var surveyURL:URL?

    @IBOutlet weak var surveyTblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        surveyTblView.delegate = self
        surveyTblView.dataSource = self
        surveyTblView.tableFooterView = UIView()
        
//        var frameworkBundle:Bundle? {
//            let bundleId = "com.sogo.mobileframework.MobileFramework"
//            return Bundle(identifier: bundleId)
//        }
        
//        surveyTblView.register(UINib(nibName:"SurveyCell", bundle: frameworkBundle), forCellReuseIdentifier: "SurveyCell")
        
        surveyTblView.register(UINib(nibName: "SurveyButtonCell", bundle: Bundle(for: SurveyButtonCell.self)), forCellReuseIdentifier: "SurveyButtonCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return surveyList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SurveyButtonCell") as! SurveyButtonCell
        cell.surveyBtn.tag = indexPath.row
        cell.surveyBtn.setTitle(surveyList[indexPath.row].surveyName, for: .normal)
        cell.surveyBtn.addTarget(self, action: #selector(btnSurveyTapped), for: .touchUpInside)
        return cell
    }
    
    @objc func btnSurveyTapped(_ sender: UIButton ) {
           let indexx = sender.tag
        self.surveyURL = surveyList[indexx].surveyURL
        performSegue(withIdentifier: "open_participation", sender: SurveyListViewController.self)
          
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "open_participation" {
               let vc = segue.destination as? WekkitViewController
            vc?.urlString = self.surveyURL
           }
           
     }
}


import UIKit

class CustomAlertViewController : UIViewController {
    let transitioner = CAVTransitioner()
    var message:String = ""
    var submitBtnText = "Ok"
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var submitBtnName: UIButton!
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self.transitioner
        
        messageLabel.text = message
        submitBtnName.setTitle(submitBtnText, for: .normal)
        
    }
    
    convenience init() {
        self.init(nibName:nil, bundle:Bundle(for: CustomAlertViewController.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    @IBAction func doDismiss(_ sender:Any) {
        self.presentingViewController?.dismiss(animated: true)
    }
}

//
//  SingleQuestionPopUp.swift
//  MobileFramework
//
//  Created by Rohit Mane on 12/4/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import UIKit

class SingleQuestionPopUp: UIViewController{
    
    @IBOutlet weak var questionTitleLbl: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var optionsTableView: UITableView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    let transitioner = CAVTransitioner()
    var questionType :String = "R"
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self.transitioner
        
//        messageLabel.text = message
//        submitButtonLabel.setTitle(submitBtnName, for: .normal)
        
    }
    
    convenience init() {
        self.init(nibName:nil, bundle:Bundle(for: SingleQuestionPopUp.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        optionsTableView.delegate = self
        optionsTableView.dataSource = self
        optionsTableView.tableFooterView = UIView()
        
        optionsTableView.register(UINib(nibName: "RadioButtonTableViewCell", bundle: Bundle(for: RadioButtonTableViewCell.self)), forCellReuseIdentifier: "RadioButtonTableViewCell")
        
      
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true)
    }
    @IBAction func btnSubmitTapped(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true)
    }
}

extension SingleQuestionPopUp : UITableViewDelegate,UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  questionType == "R" {
            return 3
        }else if questionType == "T"{
            return 1
        }
        else{
            return 1 
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if questionType == "R" {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RadioButtonTableViewCell") as? RadioButtonTableViewCell{
                
                cell.optionLabel.text = "Male"
                
                //          cell.btnDeleteSurvey.addTarget(self, action: #selector(btnDeleteSurveyTapped), for: .touchUpInside)
                return cell
            }
        }else if questionType == "T" {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "TextViewCell") as? TextViewCell{
                
                cell.textView.text = "Type here"
                
                //          cell.btnDeleteSurvey.addTarget(self, action: #selector(btnDeleteSurveyTapped), for: .touchUpInside)
                return cell
            }
        }
        else{
            
        }
       
        
        return UITableViewCell()
    }
}

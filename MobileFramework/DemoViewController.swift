//
//  DemoViewController.swift
//  MobileFramework
//
//  Created by Rohit Mane on 7/24/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import UIKit

public class DemoViewController: UIViewController {
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var btnSingleSurvey: UIButton!
    
//    var surveyList : [Survey] = [Survey.init(surveyName: "Survey Name", surveyURL: "https://survey.sogosurvey.com/r/ywEyRu",surveyButton : "Survey1" ),Survey.init(surveyName: "Survey Name", surveyURL: "https://survey.sogosurvey.com/r/UzrC8G",surveyButton : "Survey2" ),Survey.init(surveyName: "Survey Name", surveyURL: "https://survey.sogosurvey.com/r/fcPwKy",surveyButton : "Survey3" ),Survey.init(surveyName: "Survey Name", surveyURL: "https://survey.sogosurvey.com/r/bsF1z7",surveyButton : "Survey4" )]
    var selectedBtn: Int?
    var surveyURL : String?
    override public func viewDidLoad() {
        super.viewDidLoad()
        btnSingleSurvey.layer.cornerRadius = 5
        
        tableview.delegate = self
        tableview.dataSource = self
        tableview.tableFooterView = UIView()
        var frameworkBundle:Bundle? {
            let bundleId = "com.sogo.mobileframework.MobileFramework"
            return Bundle(identifier: bundleId)
        }
        tableview.register(UINib(nibName:"TableViewCell", bundle: frameworkBundle), forCellReuseIdentifier: "TableViewCell")
    }
    @IBAction func openParticipationPage(_ sender: Any) {
        performSegue(withIdentifier: "open_participation", sender: DemoViewController.self)
    }
    
    @IBAction func btnSingleSurveyTapped(_ sender: Any) {
         self.surveyURL = "https://survey.sogosurvey.com/r/ywEyRu"
         performSegue(withIdentifier: "open_participation", sender: DemoViewController.self)
    }
    
}

extension DemoViewController : UITableViewDataSource, UITableViewDelegate{
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "case 2:"
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return surveyList.count
        return 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        cell.btnSurvey.layer.cornerRadius = 5
        //cell.btnSurvey.setTitle(surveyList[indexPath.row].surveyButton, for: [])
        cell.btnSurvey.tag = indexPath.row
        cell.btnSurvey.addTarget(self, action: #selector(btnSurveyTapped), for: .touchUpInside)
        return cell
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @objc func btnSurveyTapped(_ sender: UIButton ) {
           let indexx = sender.tag
        //self.surveyURL = surveyList[indexx].surveyURL
        performSegue(withIdentifier: "open_participation", sender: DemoViewController.self)
          
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "open_participation" {
               let vc = segue.destination as? WebViewController
               vc?.surveyURL = self.surveyURL
           }
           
     }
    
    
    
    

}

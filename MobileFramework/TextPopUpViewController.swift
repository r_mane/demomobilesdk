//
//  TextPopUpViewController.swift
//  MobileFramework
//
//  Created by Rohit Mane on 11/30/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import UIKit

class TextPopUpViewController: UIViewController {
    
    var message:String = ""
    var submitBtnName:String = "OK"
    
    
    let transitioner = CAVTransitioner()
    @IBOutlet weak var feedbackTextView: UITextView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var submitButtonLabel: UIButton!
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self.transitioner
        
        messageLabel.text = message
        submitButtonLabel.setTitle(submitBtnName, for: .normal)
        
    }
    
    convenience init() {
        self.init(nibName:nil, bundle:Bundle(for: TextPopUpViewController.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    @IBAction func submitAndDismiss(_ sender:Any) {
        self.presentingViewController?.dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
         
        feedbackTextView.layer.borderWidth = 1
        feedbackTextView.layer.cornerRadius = 5.0
        feedbackTextView.layer.borderColor =  borderColor.cgColor

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

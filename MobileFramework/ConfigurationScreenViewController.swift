//
//  ConfigurationScreenViewController.swift
//  MobileFramework
//
//  Created by Rohit Mane on 11/10/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import UIKit
import WebKit

public class ConfigurationScreenViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WKUIDelegate {

    @IBOutlet weak var configurableSwitch: UISwitch!
    @IBOutlet weak var surveyNameTextField: UITextField!
    @IBOutlet weak var surveyLinkTextField: UITextField!
    @IBOutlet weak var addSurveyView: UIView!
    @IBOutlet weak var surveyTableView: UITableView!
    var webView: WKWebView!
    @IBOutlet weak var displayView: UIView!
    var surveyList = [Survey]()
    
    var plistPathInDocument: String = String()
    
    var urlString = URL(string: "https://survey.sogosurvey.com/r/ywEyRu")
    var surveyURL:String?
    public var archiveURL: URL?
    
    @IBAction func configurationSwitchValueChanged(_ sender: Any) {
        if configurableSwitch.isOn{
            addSurveyView.isHidden = false
            UserDefaults.standard.setValue(true, forKey: "switch_state")
        }else{
            addSurveyView.isHidden = true
            UserDefaults.standard.setValue(false, forKey: "switch_state")
        }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        surveyTableView.delegate = self
        surveyTableView.dataSource = self
        surveyTableView.tableFooterView = UIView()
        
        var frameworkBundle:Bundle? {
            let bundleId = "com.sogo.mobileframework.MobileFramework"
            return Bundle(identifier: bundleId)
        }
        
        surveyTableView.register(UINib(nibName: "AddedSurveyCell", bundle: Bundle(for: AddedSurveyCell.self)), forCellReuseIdentifier: "AddedSurveyCell")
        
//      surveyTableView.register(UINib(nibName:"SurveyListTableViewCell", bundle: frameworkBundle), forCellReuseIdentifier: "SurveyListTableViewCell")
        
        
        let webConfiguration = WKWebViewConfiguration()
        let customFrame = CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.displayView.frame.size.width, height: self.displayView.frame.size.height))
        self.webView = WKWebView (frame: customFrame , configuration: webConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.displayView.addSubview(webView)
        webView.topAnchor.constraint(equalTo: displayView.topAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: displayView.rightAnchor).isActive = true
        webView.leftAnchor.constraint(equalTo: displayView.leftAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: displayView.bottomAnchor).isActive = true
        webView.heightAnchor.constraint(equalTo: displayView.heightAnchor).isActive = true
        webView.uiDelegate = self
        
        
        if let data = UserDefaults.standard.value(forKey:"surveyList") as? Data {
               let decodedSports = try? PropertyListDecoder().decode([Survey].self, from: data)
               surveyList = decodedSports!
               surveyTableView.reloadData()
        }
        
//        let fileManager = FileManager.default
//        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
//        let path = documentDirectory.appending("/surveydata.plist")
//        print(path)
//        if (!fileManager.fileExists(atPath: path)) {
//            let data : [String: Any] = [
//                "surveys": []
//            ]
//            let someData = NSDictionary(dictionary: data)
//            someData.write(toFile: path, atomically: true)
//        } else {
//            print("file exists")
//        }
        
//        let ada:NSDictionary = loadDataPlist()
//        if let surveySet = ada["surveys"] as? [[String: Any]] {
//            surveyList = surveySet.map { Survey(data: $0)! }
//
//            performSegue(withIdentifier: "open_survey_list", sender: ConfigurationScreenViewController.self)
//        }
        
        
    
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return surveyList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddedSurveyCell") as! AddedSurveyCell
        cell.surveyURLLbl.text = surveyList[indexPath.row].surveyOriginalUrl
        cell.surveyNameLbl.text = surveyList[indexPath.row].surveyName
        cell.btnDeleteSurvey.addTarget(self, action: #selector(btnDeleteSurveyTapped), for: .touchUpInside)
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @objc func btnDeleteSurveyTapped(_ sender:UIButton){
        let index = sender.tag
        surveyList.remove(at: index)
        surveyTableView.reloadData()
        
    }
    
    
    @IBAction func btnAddSurveyTapped(_ sender: Any) {
        
        let surveyName = surveyNameTextField.text
        let surveyURL = surveyLinkTextField.text
        if(surveyName != "" &&  surveyURL != ""){
            if(surveyList.count < 10){
                
                do {
                    let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:true)
                    let fileURL = documentDirectory.appendingPathComponent(surveyName!).appendingPathExtension("webarchive")
                    self.archiveURL = fileURL
                } catch {
                    print(error)
                }
                
                
//                do{
//                    self.archiveURL = try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
//                    .appendingPathComponent(surveyName!).appendingPathExtension("webarchive")
//                }catch{
//                    print(error)
//                }
                
                urlString = URL(string: surveyURL ?? "https://survey.sogosurvey.com/r/ywEyRu")
                let urlRequest = URLRequest(url: urlString!)
                
                if !Reachability.isConnectedToNetwork() && archiveURL!.path != "" {
                    showAlert(msg: "Please check your net connectivity")
                }else{
                    
                    let myRequest = URLRequest(url: urlString!)
                    webView.load(myRequest)
                    webView.allowsBackForwardNavigationGestures = true

//                    //activityIndicator.startAnimating()
                    archive(surveyName: surveyName!, surveyOriginalURL: surveyURL!)
                }
            }else {
                print("can not add more than 10 surveys")
                showAlert(msg: "You can not add more than 10 surveys")
            }
        }
        
    }
    
    func archive(surveyName: String,surveyOriginalURL: String) {
        guard let url = webView.url else {
            return
        }

        if #available(iOS 11.0, *) {
            webView.configuration.websiteDataStore.httpCookieStore.getAllCookies { cookies in

                WebArchiver.archive(url: url, cookies: cookies) { result in

                    if let data = result.plistData {
                        do {
                            try data.write(to: self.archiveURL!)
                            print("web page is archived")
                            self.surveyList.append(Survey(surveyName: surveyName, surveyURL: self.archiveURL!, surveyOriginalUrl:surveyOriginalURL))
                            
//                            let encodedData = NSKeyedArchiver.archivedData(withRootObject: self.surveyList)
//                            UserDefaults.standard.set(encodedData, forKey: "surveyList")
////                            let surveyData = NSKeyedArchiver.archivedData(withRootObject: self.surveyList)
//                            UserDefaults.standard.set(self.surveyList, forKey: "surveyList")
                            
//                            let placesData = NSKeyedArchiver.archivedData(withRootObject: self.surveyList)
                            UserDefaults.standard.set(try? PropertyListEncoder().encode(self.surveyList), forKey: "surveyList")
                            
                            
                            self.saveSurveytoPlist(surveyList: self.surveyList)
                            
                            do{
                                try surveyName.write(toFile: self.plistPathInDocument, atomically: true, encoding: String.Encoding.utf8 )
                            }catch{
                                print(error)
                            }
                            
                            self.surveyTableView.reloadData()
                            self.surveyNameTextField.text = ""
                            self.surveyLinkTextField.text = ""
                            self.showAlert(msg: "Your survey is archived")
                        } catch {

                        }
                    } else if let firstError = result.errors.first {

                    }
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func loadDataPlist() -> NSMutableDictionary {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentsDirectory = paths.object(at: 0)as! NSString
        let path = documentsDirectory.appendingPathComponent("/surveydata.plist")
        return NSMutableDictionary(contentsOfFile: path)!
    }
    
    func saveSurveytoPlist(surveyList : [Survey]) {
        let a = NSMutableArray()
        
        for i in 0..<surveyList.count{
            
            let dic = NSMutableDictionary()
            dic.setObject(surveyList[i].surveyName ?? "", forKey: "surveyName" as NSCopying)
            dic.setObject(surveyList[i].surveyOriginalUrl ?? "", forKey: "surveyOriginalUrl" as NSCopying)
            dic.setObject(surveyList[i].surveyURL ?? "", forKey: "surveyURL" as NSCopying)
            a.add(dic)
        }
        
        let dicMain:NSDictionary = ["surveys":a]
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = documentDirectory.appending("/surveydata.plist")
        dicMain.write(toFile: path, atomically: true)
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        performSegue(withIdentifier: "open_survey_list", sender: ConfigurationScreenViewController.self)
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "open_survey_list" {
               let vc = segue.destination as? SurveyListViewController
               vc?.surveyList = self.surveyList
           }
     }
    
    func showAlert(msg:String) {
        let alert = UIAlertController(title: "Alert", message: msg,preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
//    public func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
////        activityIndicator.isHidden = false
////        activityIndicator.startAnimating()
//        print("Start loading")
//    }
//
//    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        print("End loading")
////        activityIndicator.isHidden = true
////        activityIndicator.stopAnimating()
//    }
    
}

//
//  WebViewController.swift
//  MobileFramework
//
//  Created by Rohit Mane on 10/6/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import Foundation
import UIKit
import WebKit

public class WebViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var urlString = URL(string: "https://sogointuc.sevenpv.com/Offline.aspx?k=SsRPRRVsUQVWsPsPsP&lang=0")
    var myTitle = "Participate"
    var surveyURL:String?
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.title = myTitle
        webView.delegate = self
         activityIndicator.isHidden = false
        //urlString = URL(string: surveyURL ?? "https://sogointuc.sevenpv.com/Offline.aspx?k=SsRPRRVsUQVWsPsPsP&lang=0")
        let urlRequest = URLRequest(url: urlString!)
        webView.loadRequest(urlRequest)
        
    }
    
    public func webViewDidStartLoad(_ webView: UIWebView) {
        //self.view.showLoader()
        activityIndicator.isHidden = false
        print("webViewDidStartLoad")
    }
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        print("webViewDidFinishLoad")
        activityIndicator.isHidden = true
        self.view.removeLoader()
        if let text = webView.request?.url?.absoluteString{
            if(text.contains("Lang") || text.contains("Status")){
                    // create the alert
                    let alert = UIAlertController(title: "Hi..", message: "Is This useful.", preferredStyle: UIAlertController.Style.alert)
        
                    // add an action (button)
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {_ in
                    self.navigationController?.popViewController(animated: false)
                }))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.destructive, handler: {_ in
                    self.navigationController?.popViewController(animated: false)
                }))
                    // show the alert
                self.present(alert, animated: true, completion: nil)
            }
            print("webView url: \(text)")
        }
        
    }
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("didFailLoadWithError")
        activityIndicator.isHidden = false
        self.view.removeLoader()

    }
    

    
}

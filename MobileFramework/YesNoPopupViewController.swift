//
//  YesNoPopupViewController.swift
//  MobileFramework
//
//  Created by Rohit Mane on 11/30/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import UIKit

class YesNoPopupViewController: UIViewController {
    
    var submitButtonName:String = "Submit"
    var yesLableName:String = "Yes"
    var noLableName: String = "No"
    var message: String = "Would us reffere our app to others ?"
    
    
    let transitioner = CAVTransitioner()
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var yesView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var yesLable: UILabel!
    @IBOutlet weak var noView: UIView!
    @IBOutlet weak var noLable: UILabel!
    @IBOutlet weak var noRadioButtonImageView: UIImageView!
    @IBOutlet weak var yesRadioButtonImageView: UIImageView!
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self.transitioner
        
        messageLabel.text = message
        submitButton.setTitle(submitButtonName, for: .normal)
        yesLable.text = yesLableName
        noLable.text = noLableName
    }
    
    convenience init() {
        self.init(nibName:nil, bundle:Bundle(for: TextPopUpViewController.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    @IBAction func submitAndDismiss(_ sender:Any) {
        self.presentingViewController?.dismiss(animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  #available(iOS 13.0, *) {
            yesRadioButtonImageView.image =  UIImage(named: "radioselected", in: Bundle(for: TextPopUpViewController.self), with: .none)
        }
        
        let yesTap = UITapGestureRecognizer(target: self, action: #selector(self.yesClicked(_:)))
        yesView.addGestureRecognizer(yesTap)
        yesView.isUserInteractionEnabled = true
        
        let noTap = UITapGestureRecognizer(target: self, action: #selector(self.noClicked(_:)))
        noView.addGestureRecognizer(noTap)
        noView.isUserInteractionEnabled = true

        // Do any additional setup after loading the view.
    }
    
    @objc func yesClicked(_ sender: UITapGestureRecognizer) {
        if #available(iOS 13.0, *) {
            yesRadioButtonImageView.image =  UIImage(named: "radioselected", in: Bundle(for: TextPopUpViewController.self), with: .none)
            noRadioButtonImageView.image = UIImage(named: "radiobutton", in: Bundle(for: TextPopUpViewController.self), with: .none)
        }
    }
    
    @objc func noClicked(_ sender: UITapGestureRecognizer) {
        if #available(iOS 13.0, *) {
            yesRadioButtonImageView.image = UIImage(named: "radiobutton", in: Bundle(for: TextPopUpViewController.self), with: .none)
            noRadioButtonImageView.image = UIImage(named: "radioselected", in: Bundle(for: TextPopUpViewController.self), with: .none)
        }
    }

//    public func returnImage(_ named:String) -> UIImage {
//        let myBundle = Bundle.init(identifier: "com.sogo.mobileframework.MobileFramework")
//        let imagePath = (myBundle?.path(forResource: "images", ofType: "bundle"))! + "/" + named
//        let theImage = UIImage(contentsOfFile: imagePath)
//        return theImage!
//    }
//
//    public func returnKernel(_ named:String) -> String {
//        let myBundle = Bundle.init(identifier: "com.sogo.mobileframework.MobileFramework")
//        let kernelPath = (myBundle?.path(forResource: "cikernels", ofType: "bundle"))! + "/" + named + ".cikernel"
//        do {
//            return try String(contentsOfFile: kernelPath)
//        }
//        catch let error as NSError {
//            return error.description
//        }
//    }
}

//
//  Survey.swift
//  MobileFramework
//
//  Created by Rohit Mane on 8/5/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import Foundation

public class Survey: Codable {
    var surveyName : String?
    var surveyURL : URL?
    var surveyOriginalUrl : String?
    
    init(surveyName : String , surveyURL: URL, surveyOriginalUrl :String) {
        self.surveyName = surveyName
        self.surveyURL = surveyURL
        self.surveyOriginalUrl = surveyOriginalUrl
    }
}

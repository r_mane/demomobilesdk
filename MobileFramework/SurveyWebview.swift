//
//  SurveyWebview.swift
//  MobileFramework
//
//  Created by Rohit Mane on 12/3/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import Foundation
import UIKit
import WebKit

public class SurveyWebview: UIViewController,WKNavigationDelegate{
    
    var webView: WKWebView!
    var urlString : URL = URL(string: "https://sogointuc.sevenpv.com/Offline.aspx?k=SsRPRRVsUQXYsPsPsP&lang=0")!
    var myTitle = "Participate"

    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.title = myTitle
        if((urlString) != nil){
            webView.load(URLRequest(url: urlString))
        }
        
    }
    
    override public func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self as! WKNavigationDelegate
        view = webView
    }
}

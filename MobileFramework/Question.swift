//
//  Question.swift
//  MobileFramework
//
//  Created by Rohit Mane on 12/4/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import Foundation

class Question {
    var zarca_q_id: String?
    var QuestionText: String?
    var QuestionType: String?
    var answers = [Answer]()
}

class Answer {
   
    var ans_id : String?
    var ansText: String?
   
}

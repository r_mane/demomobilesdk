//
//  AddedSurveyCell.swift
//  MobileFramework
//
//  Created by Rohit Mane on 11/10/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import UIKit

class AddedSurveyCell: UITableViewCell {

    @IBOutlet weak var surveyNameLbl: UILabel!
    @IBOutlet weak var surveyURLLbl: UILabel!
    @IBOutlet weak var btnDeleteSurvey: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

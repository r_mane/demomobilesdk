//
//  RadioButtonTableViewCell.swift
//  MobileFramework
//
//  Created by Rohit Mane on 12/4/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import UIKit

class RadioButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var radioImage: UIImageView!
    @IBOutlet weak var optionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

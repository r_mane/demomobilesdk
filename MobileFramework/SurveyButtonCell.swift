//
//  SurveyButtonCell.swift
//  MobileFramework
//
//  Created by Rohit Mane on 11/10/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import UIKit

class SurveyButtonCell: UITableViewCell {

    @IBOutlet weak var surveyBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
